DOCKER IMAGE FOR BOLT CMS AND COMPOSER
======================================

A docker image to run composer command line on [Bolt CMS](https://www.bolt.cm). Based on Alpine Linux and Php 7.1.

The image come with the following php libraries:

- Curl
- Gd
- Intl
- Xml
- Pdo Mysql
- mbstring
- opcache
- posix
- fileinfo
- exif
